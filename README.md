University of Dayton

Department of Computer Science

CPS 491 - Spring 2020

Dr. Phu Phung

# Capstone II Proposal

Indicator weights/heat scores - GE Aviation

## Team members
Brian Taylor, taylorb1@udayton.edu

Robert Stevenson, stevensonr2@udayton.edu

Jon Henry, henryj14@udayton.edu

## Company Mentors

Eric Gero, GE - Aviation

GE - Aviation

River Park Drive, Dayton, Ohio 45479

## Project homepage
https://cps491s20-team8.bitbucket.io/

## Overview

Currently GE uses a system in which 
employee events are categorized and given a heat score.
For example an employee sending an email to a competitor with an attachment or
Employees accessing systems they do not normally need access to. However 
the heat scores currently hard-coded and static.
This is a problem because situations can change fast, or events can be related to significantly impact the risk.

![diagram](diagram.png "diagram")

## Project Context and Scope

We plan to use a neural network and obfuscated data to generate heat scores that 
are more indictive to the individual preseved risk.

## High-level Requirements
Neural network can inport the needed data.

Neural network can adjust the heat score.

Neural network can output adjusted heat score.

Neural network posseses a api for ease of use.

## Technology

Neural Networks
* Bayesian 
* Recurrent

Languages
* Python - Tensorflow, pytorch
* C++ - opencv


## Project Management

Jan 14 - Jan 22 Sprint 0 

Jan 23 - Feb 12 Sprint 1 

Feb 13 - March 3 Sprint 2 

March 3 - Apr 1 Sprint 3

Apr 2 - Apr 22 Sprint 4

Apr 23 - May 4 project closure


trello
https://trello.com/b/zyP2VdJG/capstone2

bitbucket
https://bitbucket.org/cps491s20-team8/capstone-2

## Company Support

Meating time 3:30 every other week

Sample data